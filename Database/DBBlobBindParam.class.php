<?php
namespace v3\kernel\Database;
/**
 * Class DBBlobBindParam
 * @author    Sergey Surkov <syrkov@softkey.ru>
 * @copyright Copyright &copy; 2015 Sergey Surkov
 * @package   v3\kernel\Database
 * @version   1.0
 * @since     1.0
 */

class DBBlobBindParam extends DBBindParam
{
    /**
     * Тип данных для Bind_by_name = OCI_B_BLOB
     * @var int
     */
    private $_type = 113;

    /**
     * Возврашает значение константы OCI_B_BLOB
     * @return int
     */
    public function getType()
    {
        return $this->_type;
    }

    /**
     * Возвращает длину данных неограниченную
     * @return int
     */
    public function getMaxLength()
    {
        return -1;
    }

}