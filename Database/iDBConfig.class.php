<?php
namespace v3\kernel\Database;
/**
 * Interface iDBConfig
 * @author    Sergey Surkov <syrkov@softkey.ru>
 * @copyright Copyright &copy; 2015 Sergey Surkov
 * @package   v3\kernel\Database
 * @version   1.0
 * @since     1.0
 */
interface iDBConfig
{
    // Установка
    public function setHost($Host);

    public function setLogin($Login);

    public function setPassword($Password);

    public function setName($Name);

    public function setType($Type);

    public function setCharacter($Character);

    public function setSessionMode($SessionMode);

    public function setDebug($Debug);

    public function setDebugToFile($DebugToFile);

    // Получение
    public function getHost();

    public function getLogin();

    public function getPassword();

    public function getName();

    public function getType();

    public function getCharacter();

    public function getSessionMode();

    public function getDebug();

    public function getDebugToFile();
}