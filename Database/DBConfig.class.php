<?php
namespace v3\kernel\Database;
/**
 * Class DBConfig
 * @author    Sergey Surkov <syrkov@softkey.ru>
 * @copyright Copyright &copy; 2015 Sergey Surkov
 * @package   v3\kernel\Database
 * @version   1.0
 * @since     1.0
 */
class DBConfig implements iDBConfig
{
    /**
     * @var string
     */
    protected $Host = '';
    /**
     * @var string
     */
    protected $Login = '';
    /**
     * @var string
     */
    protected $Password = '';
    /**
     * @var string
     */
    protected $Name = '';
    /**
     * @var string
     */
    protected $Type = 'oracle';
    /**
     * @var string
     */
    protected $Character = 'AL32UTF8';
    /**
     * @var null
     */
    protected $SessionMode = null;
    /**
     * @var bool
     */
    protected $Debug = true;
    /**
     * @var bool
     */
    protected $DebugToFile = false;

    /**
     * @param $Host
     *
     * @return $this
     */
    public function setHost($Host)
    {
        if (is_string($Host)) {
            $this->Host = $Host;
        }

        return $this;
    }

    /**
     * @param $Login
     *
     * @return $this
     */
    public function setLogin($Login)
    {
        $this->Login = (string)$Login;

        return $this;

    }

    /**
     * @param $Password
     *
     * @return $this
     */
    public function setPassword($Password)
    {
        $this->Password = (string)$Password;

        return $this;

    }

    /**
     * @param $Name
     *
     * @return $this
     */
    public function setName($Name)
    {
        $this->Name = (string)$Name;

        return $this;
    }

    /**
     * @param $Type
     *
     * @return $this
     */
    public function setType($Type)
    {
        if (empty($Type)) {
            $this->Type = 'oracle';
        } else {
            $this->Type = (string)$Type;
        }

        return $this;

    }

    /**
     * @param $Character
     *
     * @return $this
     */
    public function setCharacter($Character)
    {
        if (empty($Character)) {
            $this->Character = 'AL32UTF8';
        } else {
            $this->Character = (string)$Character;
        }

        return $this;
    }

    /**
     * @param $SessionMode
     *
     * @return $this
     */
    public function setSessionMode($SessionMode)
    {
        if (!empty($SessionMode)) {
            $this->SessionMode = $SessionMode;
        }

        return $this;
    }

    /**
     * @param $Debug
     *
     * @return $this
     */
    public function setDebug($Debug)
    {
        if (empty($Debug)) {
            $this->Debug = false;
        } elseif (strtolower($Debug) == 'n') {
            $this->Debug = false;
        } else {
            $this->Debug = true;
        }

        return $this;
    }

    /**
     * @param $DebugToFile
     *
     * @return $this
     */
    public function setDebugToFile($DebugToFile)
    {
        $this->DebugToFile = trim($DebugToFile);

        return $this;
    }
    // Получение
    /**
     * @return string
     */
    public function getHost()
    {
        return $this->Host;
    }

    /**
     * @return string
     */
    public function getLogin()
    {
        return $this->Login;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->Password;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->Name;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->Type;
    }

    /**
     * @return string
     */
    public function getCharacter()
    {
        return $this->Character;
    }

    /**
     * @return null
     */
    public function getSessionMode()
    {
        return $this->SessionMode;
    }

    /**
     * @return bool
     */
    public function getDebug()
    {
        return $this->Debug;
    }

    /**
     * @return bool
     */
    public function getDebugToFile()
    {
        return $this->DebugToFile;
    }
}