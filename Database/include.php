<?php

require_once($DOCUMENT_ROOT . "/kernel/Database/DBException.class.php");
require_once($DOCUMENT_ROOT . "/kernel/Database/iDBConfig.class.php");
require_once($DOCUMENT_ROOT . "/kernel/Database/DBConfig.class.php");
require_once($DOCUMENT_ROOT . "/kernel/Database/DBConnect.class.php");
require_once($DOCUMENT_ROOT . "/kernel/Database/DBPool.class.php");
require_once($DOCUMENT_ROOT . "/kernel/Database/DBQuery.class.php");
require_once($DOCUMENT_ROOT . "/kernel/Database/DBResult.class.php");
require_once($DOCUMENT_ROOT . "/kernel/Database/iDBBindParam.class.php");
require_once($DOCUMENT_ROOT . "/kernel/Database/DBBindParam.class.php");
require_once($DOCUMENT_ROOT . "/kernel/Database/DBBlobBindParam.class.php");
require_once($DOCUMENT_ROOT . "/kernel/Database/DBClobBindParam.class.php");
require_once($DOCUMENT_ROOT . "/kernel/Database/DBDateBindParam.class.php");

use \v3\kernel\Database\DBConfig;
use \v3\kernel\Database\DBConnect;
use \v3\kernel\Database\DBPool;

// Подключение по умолчанию
$DBDefault = new DBConfig();
$DBDefault->setHost('')
    ->setLogin($DB_User)
    ->setPassword($DB_Pass)
    ->setName($DB_Name)
    ->setType('oracle')
    ->setCharacter($DB_Character)
    ->setSessionMode($DB_SessionMode)
    ->setDebug(true)
    ->setDebugToFile(false);
// Резервное подключение
$DBReserve = new DBConfig();
$DBReserve->setHost('')
    ->setLogin($DB_User_Reserve)
    ->setPassword($DB_Pass_Reserve)
    ->setName($DB_Name_Reserve)
    ->setType('oracle')
    ->setCharacter($DB_Character)
    ->setSessionMode($DB_SessionMode_Reserve)
    ->setDebug(true)
    ->setDebugToFile(false);

// Добавление соединений
DBPool::adddefault(DBConnect::create($DBDefault, $DBReserve));








