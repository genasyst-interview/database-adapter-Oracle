<?php
namespace v3\kernel\Database;
    /**
     * Class DBBindParam
     * @author    Sergey Surkov <syrkov@softkey.ru>
     * @copyright Copyright &copy; 2015 Sergey Surkov
     * @package   v3\kernel\Database
     * @version   1.0
     * @since     1.0
     */
/**
 * Class DBBindParam
 * @package v3\kernel\Database
 */
class DBBindParam implements iDBBindParam
{
    /**
     * Переменная для хранения значения тип принимает наследованный
     * @var $_value
     */
    protected $_value = null;
    /**
     * Переменная для ограничения слины данных в Bind запроса
     * @var $_max_length
     */
    private $_max_length = false;

    /**
     * Конструктор принимает значение и записывает в переменную
     *
     * @param $value
     */
    public function __construct($value)
    {
        $this->_value = $value;
    }

    /**
     * Функция принимает значение создает объкет класса и возвращает его
     *
     * @param $value
     *
     * @return static
     */
    static public function value($value)
    {
        return new static($value);
    }

    /**
     * Установка ораничения длины данных для Bind запроса
     *
     * @param $int
     *
     * @return $this
     */
    public function setMaxLength($int)
    {
        $this->_max_length = (int)$int;

        return $this;
    }

    /**
     * Функция возвращает значение value
     * @return null
     */
    public function getValue()
    {
        return $this->_value;
    }

    /**
     * Возвращает длину данных
     * @return bool
     */
    public function getMaxLength()
    {
        return $this->_max_length;
    }
}