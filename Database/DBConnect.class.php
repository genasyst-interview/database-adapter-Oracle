<?php
namespace v3\kernel\Database;
    /**
     * $connect1 = DBConnect::create($config, $config_reserve);
     * if($resource_connect = $connect1->Connect()->getConnect()) {
     *        //// query
     * }
     * ИЛИ
     * $connect2 = DBConnect::create($config, $config_reserve)->Connect();
     * if($resource_connect = $connect2->getConnect()) {
     *        //// query
     * }
     **/
/**
 * Class DBConnect
 * @author    Sergey Surkov <syrkov@softkey.ru>
 * @copyright Copyright &copy; 2015 Sergey Surkov
 * @package   v3\kernel\Database
 * @version   1.0
 * @since     1.0
 **/
class DBConnect
{

    /**
     * @var bool $debug Флаг отладки соединения
     * @access public
     */
    public $debug = false;
    /**
     * @var null $db_Conn Ресурс соединения
     * @access public
     */
    private $db_Conn = null;
    /**
     * @var
     * @access private
     */
    private $_DBDefault;
    /**
     * @var
     * @access private
     */
    private $_DBReserve;

    /**
     * @access public
     * @return void
     *
     * @param $DBDefault
     * @param $DBReserve
     */
    public function __construct(DBConfig $DBDefault, DBConfig $DBReserve = null)
    {
        $this->_DBDefault = $DBDefault;
        $this->_DBReserve = $DBReserve;
    }

    /**
     * @param DBConfig $DBDefault
     * @param DBConfig $DBReserve
     *
     * @access public
     * @return static
     */
    static public function create(DBConfig $DBDefault, DBConfig $DBReserve = null)
    {
        return new static($DBDefault, $DBReserve);
    }

    /**
     * @access public
     * @return $this
     * @throws DBException
     */
    public function connect()
    {
        if (!defined(DBPersistent)) {
            define("DBPersistent", false);
        }
        $count = 4;
        $state = "logon";
        for ($connects = 1, $executes = 1; $connects <= $count && $executes <= $count;) {
            //Если есть резервное подключение и не смогли соединиться первый раз с базой
            if (is_object($this->_DBReserve) && $connects > 1) {
                $DBConfig = $this->_DBReserve;
            } else {
                $DBConfig = $this->_DBDefault;
            }
            $this->debug = $DBConfig->getDebug();
            // logon
            if ($state == "logon") {
                $connDataStr = "User: {$DBConfig->getLogin()} ,
				 Database:  {$DBConfig->getName()} ,
				  Encoding:  {$DBConfig->getCharacter()} ,
				  SessMode: {$DBConfig->getSessionMode()} .";
                if (DBPersistent) {
                    $this->db_Conn = oci_pconnect($DBConfig->getLogin(), $DBConfig->getPassword(), $DBConfig->getName(), $DBConfig->getCharacter(), $DBConfig->getSessionMode());
                } else {
                    $this->db_Conn = oci_connect($DBConfig->getLogin(), $DBConfig->getPassword(), $DBConfig->getName(), $DBConfig->getCharacter(), $DBConfig->getSessionMode());
                }
                // Если не подключились, делаем другую попытку
                if (!is_resource($this->db_Conn)) {
                    $connects++;
                    if ($connects <= $count) {
                        usleep(30);
                        continue;
                    }
                    $error = oci_error();
                    $error_message = '';
                    if (is_array($error)) {
                        foreach ($error as $k => $v) {
                            $error_message .= "$k: $v; ";
                        }
                    }
                    if (strlen($error_message) > 0)
                        throw new DBException($error_message . ' ' . $connDataStr . ' (line ' . __LINE__ . ')');
                    else
                        throw new DBException('OCI: Error database logon. ' . $connDataStr . ' (line ' . __LINE__ . ')');

                } else {
                    $state = "parse";//ok
                }
            }
            // test query
            if ($state == "parse") {
                if (!($pr_result = oci_parse($this->db_Conn, "SELECT 1 FROM DUAL")) || !is_resource($pr_result)) {
                    $error = oci_error($this->db_Conn);
                    $error_message = '';
                    if (is_array($error)) {
                        foreach ($error as $k => $v) {
                            $error_message .= "$k: $v; ";
                        }
                    }
                    if (strlen($error_message) > 0)
                        throw new DBException($error_message . ' (line ' . __LINE__ . ')');
                    else
                        throw new DBException('OCI: No database connection (line ' . __LINE__ . ')');
                } else {
                    $state = "query";
                }
            }
            // null query
            if ($state == "query") {
                if (!@oci_execute($pr_result, OCI_DEFAULT)) {
                    $executes++;
                    $error = oci_error($pr_result);

                    //ORA-24909: call in progress. Current operation cancelled
                    //ORA-24338: statement handle not executed
                    //ORA-01013: user requested cancel of current operation
                    if (in_array($error['code'], array('24909', '24338', '01013')) && $executes <= $count) {
                        usleep(30);
                        continue;
                    }
                    //ORA-03114: not connected to ORACLE
                    if ($error['code'] == '03114' && $connects <= $count) {
                        $state = "logon";
                        usleep(30);
                        continue;
                    }
                    $error_message = '';
                    if (is_array($error)) {
                        foreach ($error as $k => $v) {
                            $error_message .= "$k: $v; ";
                        }
                    }
                    if (strlen($error_message) > 0)
                        throw new DBException($error_message . ' (line ' . __LINE__ . ')');
                    else
                        throw new DBException('OCI: No database connection (line ' . __LINE__ . ')');

                } else {
                    $state = "ok";
                }
            }
            break;
        }

        return $this;
        /*if($state == 'ok') {
            return $this;
        } else {
            return false;
        }*/
    }

    /**
     * @access public
     * @return void
     */
    public function disconnect()
    {
        oci_close($this->db_Conn);
        $this->db_Conn = null;
    }

    /**
     * @return bool|resource
     */
    public function getConnect()
    {
        if (!is_resource($this->db_Conn)) {
            $this->connect();
        }
        if (is_resource($this->db_Conn)) {
            return $this->db_Conn;
        } else {
            return false;
        }
    }

}