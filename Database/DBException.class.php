<?php
namespace v3\kernel\Database;
/**
 * Class DBException
 * @author    Sergey Surkov <syrkov@softkey.ru>
 * @copyright Copyright &copy; 2015 Sergey Surkov
 * @package   v3\kernel\Database
 * @version   1.0
 * @since     1.0
 */
class DBException extends \Exception
{

}