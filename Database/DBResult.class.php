<?php
namespace v3\kernel\Database;
    /**
     * Class DBResult
     * @author    Sergey Surkov <syrkov@softkey.ru>
     * @copyright Copyright &copy; 2015 Sergey Surkov
     * @package   v3\kernel\Database
     * @version   1.0
     * @since     1.0
     */
/**
 * Class DBResult
 * @package v3\kernel\Database
 */
class DBResult
{
    /**
     * @var null
     */
    private $_result = null;

    /**
     * @param $result_statement
     */
    public function __construct($result_statement)
    {
        $this->_result = $result_statement;
    }

    public function getResultResource()
    {
        if (!is_resource($this->_result)) {
            return false;
        } else {
            return $this->_result;
        }

    }

    /**
     * @return array|bool
     */
    public function fetch()
    {
        // Проверка на ошибку запроса
        if (!is_resource($this->_result)) {
            return false;
        } else {
            $return = oci_fetch_array($this->_result, OCI_ASSOC + OCI_RETURN_NULLS);
            if (is_array($return) && !empty($return)) {
                foreach ($return as $k => $v) {
                    // Если в ответе LOB распарсиваем его в строку
                    if (is_object($v) && get_class($v) == 'OCI-Lob') {
                        $return[$k] = $v->load();
                    }
                }
            }

            return $return;
        }
    }

    /**
     * @param $name
     *
     * @return bool
     */
    public function fetchColumn($name)
    {
        // Проверка на ошибку запроса
        if (!is_resource($this->_result)) {
            return false;
        } else {
            $return = oci_fetch_array($this->_result, OCI_ASSOC + OCI_RETURN_NULLS);
            if (array_key_exists($name, $return)) {
                return $return[$name];
            } elseif (array_key_exists(strtoupper($name), $return)) {
                return $return[strtoupper($name)];
            } elseif (array_key_exists(strtolower($name), $return)) {
                return $return[strtolower($name)];
            } else {
                return false;
            }

        }
    }

    /**
     * @param bool $field
     *
     * @return array
     */
    public function fetchAll($field = false)
    {
        // Проверка на ошибку запроса
        if (!is_resource($this->_result)) {
            $this->_result = null;

            return array();
        } else {
            $return = array();
            while ($return_row = oci_fetch_array($this->_result, OCI_ASSOC + OCI_RETURN_NULLS)) {
                // Проверяем на пустоту ответа
                if (!empty($return_row)) {
                    foreach ($return_row as $k => $v) {
                        // Если в ответе BLOB распарсиваем его в строку
                        if (is_object($v) && get_class($v) == 'OCI-Lob') {
                            $return_row[$k] = $v->load();
                        }
                    }
                    //если передана колонка в ачестве ключа массива
                    if (!empty($field)) {
                        if (array_key_exists($field, $return_row)) {
                            $return[$return_row[$field]] = $return_row;
                        } elseif (array_key_exists(strtoupper($field), $return_row)) {
                            $return[$return_row[strtoupper($field)]] = $return_row;
                        } elseif (array_key_exists(strtolower($field), $return_row)) {
                            $return[$return_row[strtolower($field)]] = $return_row;
                        } else {
                            $return[] = $return_row;
                        }
                    } else {
                        $return[] = $return_row;
                    }
                }
            }

            return $return;
        }
    }

    /**
     * @return bool|object
     */
    public function fetchObject()
    {
        // Проверка на ошибку запроса
        if (!is_resource($this->_result)) {
            return false;
        } else {
            $return = oci_fetch_object($this->_result);

            return $return;
        }
    }

    /**
     * @param bool $field
     *
     * @return array
     */
    public function fetchObjectAll($field = false)
    {
        // Проверка на ошибку запроса
        if (!is_resource($this->_result)) {
            $this->result = false;

            return array();
        } else {
            $return = array();
            while (($return_row = oci_fetch_object($this->_result)) != false) {

                // Проверяем на пустоту ответа
                if (!empty($return_row)) {
                    //если передана колонка в ачестве ключа массива
                    if (!empty($field) && isset($return_row->$field)) {
                        // записываем с индексом колонки
                        $return[$return_row->$field] = $return_row;
                    } else {
                        $return[] = $return_row;
                    }
                }
            }

            return $return;
        }
    }
}