<?php
namespace v3\kernel\Database;
    /**
     * Class DBQuery
     * @author    Sergey Surkov <syrkov@softkey.ru>
     * @copyright Copyright &copy; 2015 Sergey Surkov
     * @package   v3\kernel\Database
     * @version   1.0
     * @since     1.0
     */
    /**
     * // ИСПОЛЬЗОВАНИЕ
     * // РАЗОВЫЙ ЗАПРОС БЕЗ ПАРАМЕТРОВ
     * // Вернет первую найденную запись
     * $result = DBQuery::query('select * from sk_ext_action_log')->fetch();
     * // Вернет массив всех записей
     * $result_array = DBQuery::query('select * from sk_ext_action_log')->fetchAll();
     * // Вернет массив всех записей где ключами массива будет колонка 'name'  если она есть в выборке, иначе придет нумерованный массив
     * $result = DBQuery::query('select * from sk_ext_action_log')->fetchAll('name');
     * // РАЗОВЫЙ ЗАПРОС С ПАРАМЕТРАМИ
     * $params = array(
     * 'id'=>DBBindParam::value(12123), // Вставка простых полей
     * 'name'=>DBBindParam::value('Дима Переделкин')->setMaxLength(4000),// Поле с ограниченным числом символов
     * 'description'=>DBClobBindParam::value($mnogo_teksta), // Добавление поля CLOB
     * 'text'=>DBBlobBindParam::value($mnogo_binarnogo_teksta),// Добавление поля BLOB
     * );
     * // В данном примере сформируется параметр BLOB:
     * $this->_descriptors[':text'] = oci_new_descriptor($this->getConnect(), OCI_D_LOB);
     * $res = oci_bind_by_name($this->_statement, ':text', $this->_descriptors[':text'], -1, OCI_B_CLOB);
     * $this->_descriptors[':text']->writeTemporary($value['value'], OCI_TEMP_BLOB);
     *
     * // Вернет первую найденную запись
     * $result = DBQuery::query('select * from sk_ext_action_log WHERE id=:id AND RAW_POST_DATA=:text',$params)->fetch();
     * // Вернет массив всех записей
     * $result_array = DBQuery::query('select * from sk_ext_action_log WHERE id=:id AND RAW_POST_DATA=:text',$params)->fetchAll();
     * // Вернет массив всех записей где ключами массива будет колонка 'name'  если она есть в выборке, иначе придет нумерованный массив
     * $result = DBQuery::query('select * from sk_ext_action_log WHERE id=:id AND RAW_POST_DATA=:text')->fetchAll('name');
     * // МНОЖЕСТВЕННЫЙ ЗАПРОС
     * $ids = array(1,2,3,4,5,6,7,8);
     * // Подготавливаем запрос
     * $DBQuery = DBQuery::sql('select * from sk_ext_action_log WHERE id=:id');
     * foreach($ids as $v) {
     * $params = array('id'=>$v);
     * // Получаем результат
     * $result_array[] = $DBQuery->exec($params)->fetch();
     * }
     * // УСТАНОВКА ПАРАМЕТРОВ
     * $params = array(
     * 'id'=>DBBindParam::value(12123), // Вставка простых полей
     * 'name'=>DBBindParam::value('Дима Переделкин')->maxLength(4000),// Поле с ограниченным числом символов
     * 'description'=>DBClobBindParam::value($mnogo_teksta), // Добавление поля CLOB
     * 'text'=>DBBlobBindParam::value($mnogo_binarnogo_teksta),// Добавление поля BLOB
     * 'date_insert'=>DBDateBindParam::value('SYSDATE'), Принимает встроеную функцию ORACLE (TEXT)
     * 'date_insert'=>DBDateBindParam::value(time()), // Либо принимает метку времени UNIX (INT)
     * );
     * */

/**
 * Class DBQuery
 * @package v3\kernel\Database
 */
class DBQuery
{
    /**
     * @var mixed|null
     */
    private $_DBConnect = null;
    /**
     * @var string
     */
    private $_query = '';
    /**
     * @var null
     */
    private $_statement = null;
    /**
     * @var array
     */
    private $_descriptors = array();

    /**
     * @param           $query
     * @param DBConnect $DBConnect
     */
    public function __construct($query, DBConnect $DBConnect = null)
    {
        if (!empty($query)) {
            $this->_query = $query;
        }
        if (is_object($DBConnect)) {
            $this->_DBConnect = $DBConnect;
        } else {
            if (!is_object($this->_DBConnect)) {
                $this->_DBConnect = DBPool::getDefault();
            }
        }
    }

    /**
     * @param           $query
     * @param DBConnect $DBConnect
     *
     * @return static
     */
    public static function create($query, DBConnect $DBConnect = null)
    {
        $Class = new static($query, $DBConnect);
        $Class->prepare();

        return $Class;
    }

    /**
     * @param           $query
     * @param DBConnect $DBConnect
     *
     * @return DBQuery
     */
    public static function sql($query, DBConnect $DBConnect = null)
    {
        return self::create($query, $DBConnect);
    }

    /**
     * @param       $query
     * @param array $params
     *
     * @return mixed
     */
    public static function query($query, $params = array())
    {
        $Class = new static($query);
        $Class->prepare();

        return $Class->exec($params);
    }

    /**
     * @param       $table
     * @param array $data
     *
     * @return bool|object
     */
    public static function insert($table, $data = array())
    {
        if (!empty($data)) {
            $query = "INSERT INTO " . $table . " ";
            $fields = ' ';
            $fields_data = ' ';
            foreach ($data as $k => $v) {
                if (get_class($v) == 'v3\kernel\Database\DBDateBindParam') {
                    $fields .= " " . $k . ",";
                    if (is_string($v->getValue())) {
                        $fields_data .= " " . $v->getValue() . ",";
                    } elseif (is_integer($v->getValue())) {
                        $fields_data .= "TO_DATE('" . date("Y.m.d H:i:s", $v->getValue()) . "', 'yyyy.mm.dd HH24:MI:SS'),";
                    }
                    unset($data[$k]);
                } else {
                    $fields .= " " . $k . ",";
                    $fields_data .= " :" . $k . ",";
                }
            }
            $fields = rtrim($fields, ',');
            $fields_data = rtrim($fields_data, ',');
            $query .= "(" . $fields . ") VALUES (" . $fields_data . ")";
            $result = self::query($query, $data);
            if (is_object($result)) {
                return $result;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * @param       $table
     * @param       $field
     * @param       $value
     * @param array $data
     *
     * @return bool|object
     */
    public static function updateByField($table, $field, $value, $data = array())
    {
        if (!empty($data)) {
            $query = "UPDATE " . $table . " SET";
            $fields = ' ';

            foreach ($data as $k => $v) {
                // Если тип даты то конвертируем
                if (get_class($v) == 'v3\kernel\Database\DBDateBindParam') {
                    if (is_string($v->getValue())) {
                        $fields .= $k . "= " . $v->getValue() . ",";
                    } elseif (is_integer($v->getValue())) {
                        $fields .= $k . " = TO_DATE('" . date("Y.m.d H:i:s", $v->getValue()) . "', 'yyyy.mm.dd HH24:MI:SS'),";
                    }
                    unset($data[$k]);
                } else {
                    $fields .= $k . "=:" . $k . ",";
                }

            }
            $fields = rtrim($fields, ',');
            $query .= " " . $fields . " WHERE " . $field . " = :" . $field . "";
            $data[':' . $field . ''] = DBBindParam::value($value);
            $result = self::query($query, $data);
            if (is_object($result)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }

    }

    /**
     * @param       $table
     * @param       $value
     * @param array $data
     *
     * @return bool|object
     */
    public static function updateById($table, $value, $data = array())
    {
        return self::updateByField($table, 'ID', $value, $data);
    }

    /**
     * @param      $table
     * @param      $field
     * @param null $value
     *
     * @return bool
     */
    public static function deleteByField($table, $field, $value = null)
    {
        if ($value != null) {
            $query = "DELETE FROM " . $table . " WHERE " . $field . " = :" . $field;
            $result = self::query($query, array('' . $field . '' => DBBindParam::value($value)));
            if (is_object($result)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * @param      $table
     * @param null $value
     *
     * @return bool
     */
    public static function deleteById($table, $value = null)
    {
        return self::deleteByField($table, 'ID', $value);
    }


    /**
     * @param DBConnect $DBConnect
     *
     * @throws DBException
     */
    public function setConnect(DBConnect $DBConnect = null)
    {
        if (is_object($DBConnect)) {
            $this->_DBConnect = $DBConnect;
        } else {
            throw new DBException('DBQuery::setConnect $DBConnect non object (line ' . __LINE__ . ')');
        }
    }

    /**
     * @param $name
     * @param $value
     *
     * @return bool
     * @throws DBException
     */
    public function setBindParam($name, $_value)
    {
        $value = $_value;
        // Если переданное имя без двоеточия ставим его
        $name = trim($name);
        if (substr($name, 0, 1) != ':') {
            $name = ':' . $name;
        }
        $max = false;
        $type = false;
        // Если параметр является объкетом или массивом, распарсивем его
        if (is_array($value) ||
            (is_object($value) &&
                (get_class($value) == 'v3\kernel\Database\DBBindParam' ||
                    get_class($value) == 'v3\kernel\Database\DBBlobBindParam' ||
                    get_class($value) == 'v3\kernel\Database\DBClobBindParam' ||
                    get_class($value) == 'v3\kernel\Database\DBDateBindParam')
            )
        ) {
            // Если объект классов BindParam
            if (is_object($value)) {
                // Получаем данные поля
                if (method_exists($value, 'getType')) {
                    $type = $value->getType();
                }

                $max = $value->getMaxLength();
                $value = $value->getValue();
                // Если пришел массив
            } elseif (is_array($value)) {
                // Получаем  данные поля
                if (array_key_exists('max', $value)) {
                    $max = $value['max'];
                }
                if (array_key_exists('type', $value)) {
                    $type = $value['type'];
                }
                if (array_key_exists('value', $value)) {
                    $value = $value['value'];
                }
            } else {
                $value = '';
            }

            //  Если передан тип BLOB
            if ($type == OCI_B_BLOB || strtoupper(trim($type)) == 'BLOB') {
                $this->_descriptors[$name] = oci_new_descriptor($this->getConnect(), OCI_D_LOB);
                $res = oci_bind_by_name($this->_statement, $name, $this->_descriptors[$name], -1, OCI_B_BLOB);
                $this->_descriptors[$name]->writeTemporary($value, OCI_TEMP_BLOB);

                // Если передан тип CLOB
            } elseif ($type == OCI_B_CLOB || strtoupper(trim($type)) == 'CLOB') {
                $this->_descriptors[$name] = oci_new_descriptor($this->getConnect(), OCI_D_LOB);
                //   (res-ociparse,:name,  res-oci_new_descriptor, -1, OCI_B_CLOB)
                $res = oci_bind_by_name($this->_statement, $name, $this->_descriptors[$name], -1, OCI_B_CLOB);
                $this->_descriptors[$name]->writeTemporary($value, OCI_TEMP_CLOB);

                // Если ограничена длина и передан тип
            } elseif ($max !== false && $type !== false) {
                $res = oci_bind_by_name($this->_statement, $name, $value, $max, $type);

                // Если указана длина значения
            } elseif ($max !== false) {
                $res = oci_bind_by_name($this->_statement, $name, $value, $max);

                // Если передан тип поля
            } elseif ($type !== false) {
                $res = oci_bind_by_name($this->_statement, $name, $value, -1, $type);
                // Все остальное просто записываем
            } else {

                $res = oci_bind_by_name($this->_statement, $name, $value);
            }
            // Если пришел не массив просто ставим значение
        } elseif (!is_object($value) && !is_array($value)) {
            $res = oci_bind_by_name($this->_statement, $name, $value);
        } else {
            $res = false;
        }
        if (!$res) {
            throw new DBException('DBQuery::setBindParam("' . $name . ',' . print_r($_value, true) . '") no create oci_bind_by_name($this->_statement, "' . $name . '",  ' . print_r($value, true) . ', ' . print_r($max, true) . ', ' . print_r($type, true) . '); (line ' . __LINE__ . ')');
        }
    }

    /**
     * @return string
     */
    public function getQuery()
    {
        return $this->_query;
    }

    /**
     * @throws DBException
     */
    public function prepare()
    {
        $this->_statement = oci_parse($this->getConnect(), $this->_query);
        if (!$this->_statement) {
            $err = oci_error($this->_statement);
            throw new DBException('DBQuery::prepare: ' . $err['message'] . ' (line ' . __LINE__ . ')');
        }
    }

    /**
     *
     */
    private function cleanDescriptors()
    {
        // Удаляем дескрипторы
        if (!empty($this->_descriptors)) {
            foreach ($this->_descriptors as $k => $v) {
                $v->free();
                unset($this->_descriptors[$k]);
            }
        }
    }

    /**
     * @param $params
     *
     * @return DBResult
     * @throws DBException
     */
    public function exec($params)
    {
        $mode = OCI_COMMIT_ON_SUCCESS;
        // Транзвкции будут включены в след. версии
        /*if ($this->isTransaction()) {
            if (PHP_VERSION_ID > 503020) {
                $mode = OCI_NO_AUTO_COMMIT;
            } else {
                $mode = OCI_DEFAULT;
            }
        }*/
        // Добавляем параметры в запрос
        if (is_array($params) && !empty($params)) {
            $this->setParams($params);
        }
        // Выполняем запрос
        $exec = oci_execute($this->_statement, $mode);
        $this->cleanDescriptors();
        if ($exec) {
            $result = new DBResult($this->_statement);

            return $result;
        } else {
            $e = oci_error($this->_statement);
            throw new DBException('DBQuery::exec(' . $this->getQuery() . ',' . print_r($params, true) . ') no oci_execute() = ' . $e['message']);
        }

    }

    /**
     * @param $params
     *
     * @throws DBException
     */
    private function setParams($params)
    {
        foreach ($params as $key => $value) {
            $this->setBindParam($key, $value);
        }
    }

    /**
     * @return mixed
     */
    private function getConnect()
    {
        return $this->_DBConnect->getConnect();
    }

}


