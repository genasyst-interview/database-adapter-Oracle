<?php
namespace v3\kernel\Database;
/**
 * Class DBConfig
 * @author    Sergey Surkov <syrkov@softkey.ru>
 * @copyright Copyright &copy; 2015 Sergey Surkov
 * @package   v3\kernel\Database
 * @version   1.0
 * @since     1.0
 */
interface iDBBindParam
{
    public function __construct($value);

    public static function value($value);

    public function setMaxLength($int);

    public function getValue();

    public function getMaxLength();
}