<?php
namespace v3\kernel\Database;
/**
 * Class DBPool
 * @author    Sergey Surkov <syrkov@softkey.ru>
 * @copyright Copyright &copy; 2015 Sergey Surkov
 * @package   v3\kernel\Database
 * @version   1.0
 * @since     1.0
 */
class DBPool
{
    /**
     * @var array
     */
    static private $_connections = array();
    /**
     * @var string
     */
    static private $_default_name = 'default_121';

    /**
     * @param           $name
     * @param DBConnect $DBConnect
     */
    static public function add($name, DBConnect $DBConnect)
    {
        self::$_connections[$name] = $DBConnect;
    }

    /**
     * @param DBConnect $DBConnect
     */
    static public function addDefault(DBConnect $DBConnect)
    {
        self::add(self::$_default_name, $DBConnect);
    }

    /**
     * @param $name
     *
     * @return mixed
     * @throws DBException
     */
    static public function get($name)
    {
        if (array_key_exists($name, self::$_connections)) {
            return self::$_connections[$name];
        } elseif (strtolower($name) == 'default' && array_key_exists(self::$_default_name, self::$_connections)) {
            return self::$_connections[self::$_default_name];
        } else {
            throw new DBException('Request unknown connection name "' . $name . '"! (line ' . __LINE__ . ')');
        }
    }

    /**
     * @return mixed
     */
    static public function getDefault()
    {
        return self::get(self::$_default_name);
    }
}




